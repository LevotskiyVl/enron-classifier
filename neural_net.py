import tensorflow as tf

import numpy as np
import pandas as pd
import tempfile

def train_neural_net(model_ready_data=None, model_dir=None):
    if model_ready_data is None:
        model_ready_data = pd.read_csv('enron_classification_df.csv')
    # https://stackoverflow.com/questions/38250710/how-to-split-data-into-3-sets-train-validation-and-test
    # (60% - train set, 20% - validation set, 20% - test set)
    df_train, df_test, df_val = np.split(model_ready_data.sample(frac=1),
                                         [int(.6 * len(model_ready_data)),
                                          int(.8 * len(model_ready_data))])

    # Continuous base columns
    content_length = tf.contrib.layers.real_valued_column("content_length")
    content_word_count = tf.contrib.layers.real_valued_column("content_word_count")
    subject_length = tf.contrib.layers.real_valued_column("subject_length")
    subject_word_count = tf.contrib.layers.real_valued_column("subject_word_count")
    group_LEGAL = tf.contrib.layers.real_valued_column("group_LEGAL")
    group_COMMUICATIONS = tf.contrib.layers.real_valued_column("group_COMMUICATIONS")
    group_SECURITY_SPAM_ALERTS = tf.contrib.layers.real_valued_column("group_SECURITY_SPAM_ALERTS")
    group_SUPPORT = tf.contrib.layers.real_valued_column("group_SUPPORT")
    group_ENERGY_DESK = tf.contrib.layers.real_valued_column("group_ENERGY_DESK")
    group_SALES_DEPARTMENT = tf.contrib.layers.real_valued_column("group_SALES_DEPARTMENT")
    content_length_bucket = tf.contrib.layers.bucketized_column(content_length, boundaries=[100, 200, 300, 400])
    subject_length_bucket = tf.contrib.layers.bucketized_column(subject_length, boundaries=[10, 15, 20, 25, 30])

    # Categorical base columns
    is_AM_sparse_column = tf.contrib.layers.sparse_column_with_keys(column_name="is_AM", keys=["yes", "no"])
    is_weekday_sparse_column = tf.contrib.layers.sparse_column_with_keys(column_name="is_weekday", keys=["yes", "no"])

    categorical_columns = [is_AM_sparse_column, is_weekday_sparse_column, content_length_bucket, subject_length_bucket]

    deep_columns = [content_length, content_word_count, subject_length, subject_word_count,
                    group_LEGAL, group_COMMUICATIONS, group_SECURITY_SPAM_ALERTS, group_SUPPORT,
                    group_ENERGY_DESK, group_SALES_DEPARTMENT]

    simple_columns = [group_LEGAL, group_COMMUICATIONS, group_SECURITY_SPAM_ALERTS, group_SUPPORT,
                      group_ENERGY_DESK, group_SALES_DEPARTMENT]
    if model_dir is None:
        model_dir = tempfile.mkdtemp()
    classifier = tf.contrib.learn.DNNClassifier(feature_columns=deep_columns,
                                                hidden_units=[20, 20],
                                                n_classes=6,
                                                model_dir=model_dir, )

    # Define the column names for the data sets.
    COLUMNS = ['content_length',
               'content_word_count',
               'group_LEGAL',
               'group_COMMUICATIONS',
               'group_SECURITY_SPAM_ALERTS',
               'group_SUPPORT',
               'group_ENERGY_DESK',
               'group_SALES_DEPARTMENT',
               'is_AM',
               'is_weekday',
               'subject_length',
               'subject_word_count',
               'outcome']
    LABEL_COLUMN = 'outcome'
    CATEGORICAL_COLUMNS = ["is_AM", "is_weekday"]
    CONTINUOUS_COLUMNS = ['content_length',
                          'content_word_count',
                          'group_LEGAL',
                          'group_COMMUICATIONS',
                          'group_SECURITY_SPAM_ALERTS',
                          'group_SUPPORT',
                          'group_ENERGY_DESK',
                          'group_SALES_DEPARTMENT',
                          'subject_length',
                          'subject_word_count']

    LABELS = [0, 1, 2, 3, 4, 5]

    def input_fn(df):
        # Creates a dictionary mapping from each continuous feature column name (k) to
        # the values of that column stored in a constant Tensor.
        continuous_cols = {k: tf.constant(df[k].values)
                           for k in CONTINUOUS_COLUMNS}
        # Creates a dictionary mapping from each categorical feature column name (k)
        # to the values of that column stored in a tf.SparseTensor.
        categorical_cols = {k: tf.SparseTensor(
            indices=[[i, 0] for i in range(df[k].size)],
            values=df[k].values,
            dense_shape=[df[k].size, 1])
            for k in CATEGORICAL_COLUMNS}
        # Merges the two dictionaries into one.
        # feature_cols = dict(continuous_cols.items() + categorical_cols.items())
        feature_cols = {}
        feature_cols.update(continuous_cols)
        feature_cols.update(categorical_cols)

        # Converts the label column into a constant Tensor.
        label = tf.constant(df[LABEL_COLUMN].values)
        # Returns the feature columns and the label.
        return feature_cols, label

    def train_input_fn():
        return input_fn(df_train)

    def eval_input_fn():
        return input_fn(df_test)

    # After reading in the data, you can train and evaluate the model:
    classifier.fit(input_fn=train_input_fn, steps=10000)
    results = classifier.evaluate(input_fn=eval_input_fn, steps=1)
    for key in sorted(results):
        print("%s: %s" % (key, results[key]))

    y_pred = classifier.predict(input_fn=lambda: input_fn(df_val), as_iterable=False)
    print(y_pred)
    print('buckets found:', set(y_pred))
    # # confusion matrix analysis
    # from sklearn.metrics import confusion_matrix
    # confusion_matrix(df_val[LABEL_COLUMN], y_pred)
    # pd.crosstab(df_val[LABEL_COLUMN], y_pred, rownames=['Actual'], colnames=['Predicted'], margins=True)

    # create some data
    # https://stats.stackexchange.com/questions/95209/how-can-i-interpret-sklearn-confusion-matrix
    lookup = {0: 'LEGAL', 1: 'COMMUICATIONS', 2: 'SECURITY', 3: 'SUPPORT', 4: 'ENERGY', 5: 'SALES'}
    y_truet = pd.Series([lookup[_] for _ in df_val[LABEL_COLUMN]])
    y_predt = pd.Series([lookup[_] for _ in y_pred])
    print(y_predt.values)
    pd.crosstab(y_truet, y_predt, rownames=['Actual'], colnames=['Predicted'], margins=True)

    return classifier