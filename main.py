import tensorflow as tf
import collections
import os
import random
import numpy as np
from tqdm import tqdm
import sys, email
import pandas as pd
import math
import datetime
import email
import string, re
import time
import json
import collections
from neural_net import *
from sklearn.cluster import KMeans
from classters import  *
import pickle 

ENRON_EMAIL_DATASET_PATH = 'emails.csv'
GLOVE_DATASET_PATH = 'glove.840B.300d.txt'

#Работа с датасетом
def load_dataset(path):
    # Load Enron dataset
    emails_df = pd.read_csv(path)
    emails_df.head()
    return emails_df

#обработка
def get_text_from_email(msg):
    '''To get the content from email objects'''
    parts = []
    for part in msg.walk():
        if part.get_content_type() == 'text/plain':
            parts.append( part.get_payload() )
    return ''.join(parts)

def create_emails_df(emails_df):
    messages = list(map(email.message_from_string, emails_df['message']))
    emails_df.drop('message', axis=1, inplace=True)
    # Get fields from parsed email objects
    keys = messages[0].keys()
    for key in keys:
        emails_df[key] = [doc[key] for doc in messages]
    # Parse content from emails
    emails_df['Content'] = list(map(get_text_from_email, messages))

    # keep only Subject and Content for this exercise
    emails_df = emails_df[['Date','Subject','Content']]
    return emails_df

def get_dataset_wordst(emails_df):
    words = []
    for i in tqdm(range(0, 50)):
        emails_sample_df = emails_df[i * 10000: (i+1) * 10000].copy()
        # clean up subject line
        emails_sample_df['Subject'] = emails_sample_df['Subject'].str.lower()
        emails_sample_df['Subject'] = emails_sample_df['Subject'].str.replace(r'[^a-z]', ' ')
        emails_sample_df['Subject'] = emails_sample_df['Subject'].str.replace(r'\s+', ' ')
        # clean up content line
        emails_sample_df['Content'] = emails_sample_df['Content'].str.lower()
        emails_sample_df['Content'] = emails_sample_df['Content'].str.replace(r'[^a-z]', ' ')
        emails_sample_df['Content'] = emails_sample_df['Content'].str.replace(r'\s+', ' ')
        # create sentence list
        emails_text = (emails_sample_df["Subject"] + " " + emails_sample_df["Content"]).tolist()
        sentences = ' '.join(emails_text)
        sentences = sentences.split()
        del emails_sample_df
        words = words + sentences
        #print('Data size', len(words))
    return words

def build_dataset(words):
    count = [['UNK', -1]]
    count.extend(collections.Counter(words).most_common(vocabulary_size - 1))
    dictionary = dict()
    for word, _ in count:
        dictionary[word] = len(dictionary)
    data = list()
    unk_count = 0
    for word in tqdm(words):
        if word in dictionary:
            index = dictionary[word]
        else:
            index = 0  # dictionary['UNK']
            unk_count += 1
        data.append(index)
    count[0][1] = unk_count
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    return data, count, dictionary, reverse_dictionary

def load_glove(GLOVE_DATASET_PATH, dictionary):
    embeddings_index = {}
    f = open(GLOVE_DATASET_PATH)
    word_counter = 0
    cntr = 0
    for line in tqdm(f):
        if cntr == 121896:
            continue
        cntr += 1
        values = line.split()
        word = values[0]
        if word in dictionary:
            coefs = np.asarray(values[1:], dtype='float32')
            embeddings_index[word] = coefs
        word_counter += 1
    f.close()
    return embeddings_index, word_counter


#########################################################
# Check out some clusters
#########################################################
def see_clusters(embeddings_index, CLUSTER_SIZE=50):
    # See what it learns and look at clusters to pull out major themes in the data
    # create a dataframe using the embedded vectors and attach the key word as row header
    enrond_dataframe = pd.DataFrame(embeddings_index)
    enrond_dataframe = pd.DataFrame.transpose(enrond_dataframe)

    # cluster vector and investigate top groups
    kmeans = KMeans(n_clusters=CLUSTER_SIZE)
    cluster_make = kmeans.fit_predict(enrond_dataframe)

    labels = kmeans.predict(enrond_dataframe)

    cluster_frequency = collections.Counter(labels)
    cluster_frequency.most_common()

    clusters = {}
    n = 0
    for item in tqdm(labels):
        if item in clusters:
            clusters[item].append(list(enrond_dataframe.index)[n])
        else:
            clusters[item] = [list(enrond_dataframe.index)[n]]
        n += 1

    for k, v in cluster_frequency.most_common(50):
        print('\n\n')
        print('Cluster:', k)
        print(' '.join(clusters[k]))


def prepare_data_for_training(emails_sample_df, departments):
    # loop through all emails and count group words in each raw text
    words_groups = []
    for group_id in range(len(departments)):
        work_group = []
        print('Working bag number:', str(group_id))
        top_words = departments[group_id]
        for index, row in tqdm(emails_sample_df.iterrows()):
            text = (row["Subject"] + " " + row["Content"])
            work_group.append(len(set(top_words) & set(text.split())))
            # work_group.append(len([w for w in text.split() if w in set(top_words)]))

        words_groups.append(work_group)
    # count emails per category group and feature engineering

    raw_text = []
    subject_length = []
    subject_word_count = []
    content_length = []
    content_word_count = []
    is_am_list = []
    is_weekday_list = []
    group_LEGAL = []
    group_COMMUICATIONS = []
    group_SECURITY_SPAM_ALERTS = []
    group_SUPPORT = []
    group_ENERGY_DESK = []
    group_SALES_DEPARTMENT = []
    final_outcome = []

    emails_sample_df['Subject'].fillna('', inplace=True)
    emails_sample_df['Date'] = pd.to_datetime(emails_sample_df['Date'], infer_datetime_format=True)

    counter = 0
    for index, row in tqdm(emails_sample_df.iterrows()):
        raw_text.append([row["Subject"] + " " + row["Content"]])
        group_LEGAL.append(words_groups[0][counter])
        group_COMMUICATIONS.append(words_groups[1][counter])
        group_SECURITY_SPAM_ALERTS.append(words_groups[2][counter])
        group_SUPPORT.append(words_groups[3][counter])
        group_ENERGY_DESK.append(words_groups[4][counter])
        group_SALES_DEPARTMENT.append(words_groups[5][counter])
        outcome_tots = [words_groups[0][counter], words_groups[1][counter], words_groups[2][counter],
                        words_groups[3][counter], words_groups[4][counter], words_groups[5][counter]]
        final_outcome.append(outcome_tots.index(max(outcome_tots)))

        subject_length.append(len(row['Subject']))
        subject_word_count.append(len(row['Subject'].split()))
        content_length.append(len(row['Content']))
        content_word_count.append(len(row['Content'].split()))
        dt = row['Date']
        is_am = 'no'
        if (dt.time() < datetime.time(12)): is_am = 'yes'
        is_am_list.append(is_am)
        is_weekday = 'no'
        if (dt.weekday() < 6): is_weekday = 'yes'
        is_weekday_list.append(is_weekday)
        counter += 1

    # add simple engineered features?
    training_set = pd.DataFrame({
        "raw_text": raw_text,
        "group_LEGAL": group_LEGAL,
        "group_COMMUICATIONS": group_COMMUICATIONS,
        "group_SECURITY_SPAM_ALERTS": group_SECURITY_SPAM_ALERTS,
        "group_SUPPORT": group_SUPPORT,
        "group_ENERGY_DESK": group_ENERGY_DESK,
        "group_SALES_DEPARTMENT": group_SALES_DEPARTMENT,
        "subject_length": subject_length,
        "subject_word_count": subject_word_count,
        "content_length": content_length,
        "content_word_count": content_word_count,
        "is_AM": is_am_list,
        "is_weekday": is_weekday_list,
        "outcome": final_outcome})

    # remove all emails that have all zeros (i.e. not from any of required categories)
    training_set = training_set[(training_set.group_LEGAL > 0) |
                                (training_set.group_COMMUICATIONS > 0) |
                                (training_set.group_SECURITY_SPAM_ALERTS > 0) |
                                (training_set.group_SUPPORT > 0) |
                                (training_set.group_ENERGY_DESK > 0) |
                                (training_set.group_SALES_DEPARTMENT > 0)]
    return training_set


emails_df = load_dataset(ENRON_EMAIL_DATASET_PATH)
emails_df = create_emails_df(emails_df)
words = get_dataset_wordst(emails_df)
print('Data size', len(words))
#del emails_df

vocabulary_size = 50000
data, count, dictionary, reverse_dictionary = build_dataset(words)
del words  # Hint to reduce memory.
print('Most common words (+UNK)', count[:5])
print('Sample data', data[:10], [reverse_dictionary[i] for i in data[:10]])

embeddings_index, word_counter = load_glove(GLOVE_DATASET_PATH, dictionary)
see_clusters(embeddings_index)

departments = augument_classters(GLOVE_DATASET_PATH)
with open('full_bags.pk', 'wb') as handle:
    pickle.dump(departments, handle)

training_set = prepare_data_for_training(emails_df, departments)
training_set.to_csv('enron_classification_df.csv', index=False, header=True)

train_neural_net(training_set)


