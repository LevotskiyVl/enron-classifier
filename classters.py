import scipy
import scipy.spatial
import pandas as pd
import tqdm
import numpy
####################################################
# Load master clusters for all six deparatments
####################################################

LEGAL = ['affirmed', 'alleged', 'appeal', 'appealed', 'appeals', 'appellate', 'attorney', 'attorneys', 'bankruptcy',
         'case', 'cases', 'charged', 'charges', 'civil', 'claim', 'claims', 'complaint', 'constitutional',
         'constitutionality', 'constitutionally', 'copyright', 'counsel', 'court', 'courts', 'criminal', 'damages',
         'decision', 'decree', 'decrees', 'defendants', 'denied', 'dispute', 'dissenting', 'enforcement', 'federal',
         'filed', 'filing', 'invalidate', 'invalidated', 'judge', 'judgement', 'judges', 'judgment', 'judgments',
         'judicial', 'judiciary', 'jurisdiction', 'jurisprudence', 'justice', 'justices', 'law', 'laws', 'lawsuit',
         'lawsuits', 'lawyer', 'lawyers', 'legal', 'legality', 'legally', 'litigation', 'overrule', 'overruled',
         'overturn', 'overturned', 'overturning', 'plaintiff', 'precedent', 'precedents', 'prosecutorial', 'reversed',
         'rights', 'ruled', 'ruling', 'rulings', 'settlement', 'settlements', 'sue', 'supreme', 'tribunal', 'tribunals',
         'unanimous', 'unconstitutional', 'upheld', 'uphold', 'upholding', 'upholds', 'verdict', 'violation']

COMMUICATIONS = ['accessed', 'ads', 'alphabetical', 'alphabetically', 'archive', 'archived', 'archives', 'below',
                 'bookmark', 'bookmarked', 'bookmarks', 'browse', 'browsing', 'calendar', 'catalog', 'categories',
                 'categorized', 'category', 'chart', 'charts', 'check', 'classified', 'classifieds', 'codes', 'compare',
                 'content', 'database', 'details', 'directories', 'directory', 'domain', 'domains', 'downloadable',
                 'entries', 'favorites', 'feeds', 'free', 'genealogy', 'homepage', 'homepages', 'hosting', 'index',
                 'indexed', 'indexes', 'info', 'information', 'keyword', 'keywords', 'library', 'link', 'linking',
                 'links', 'list', 'listed', 'listing', 'listings', 'lists', 'locate', 'locator', 'maps', 'online',
                 'page', 'pages', 'peruse', 'portal', 'profile', 'profiles', 'rated', 'related', 'resource', 'results',
                 'search', 'searchable', 'searched', 'searches', 'searching', 'selections', 'signup', 'site', 'sites',
                 'sorted', 'statistics', 'stats', 'subscribing', 'tagged', 'testimonials', 'titles', 'updated',
                 'updates', 'via', 'web', 'webmaster', 'webpage', 'webpages', 'website', 'websites', 'wishlist',
                 'accountant', 'careers', 'clerical', 'contracting', 'department', 'employed', 'employee', 'employees',
                 'employer', 'employers', 'employment', 'experienced', 'freelance', 'fulltime', 'generalist', 'hire',
                 'hired', 'hires', 'hiring', 'hourly', 'intern', 'interviewing', 'job', 'jobs', 'labor', 'labour',
                 'managerial', 'manpower', 'office', 'paralegal', 'personnel', 'placements', 'positions', 'profession',
                 'professional', 'professions', 'qualified', 'receptionist', 'recruit', 'recruiter', 'recruiters',
                 'recruiting', 'recruitment', 'resume', 'resumes', 'salaried', 'salaries', 'salary', 'seeking',
                 'skilled', 'staff', 'staffing', 'supervisor', 'trainee', 'vacancies', 'vacancy', 'worker', 'workers',
                 'workforce', 'workplace']

SECURITY_SPAM_ALERTS = ['abducted', 'accidental', 'anthrax', 'anti', 'antibiotic', 'antibiotics', 'assaulted',
                        'attacked', 'attacker', 'attackers', 'auth', 'authenticated', 'authentication', 'avoid',
                        'avoidance', 'avoided', 'avoiding', 'bacteria', 'besieged', 'biometric', 'bioterrorism',
                        'blocking', 'boarded', 'bodyguards', 'botched', 'captive', 'captives', 'captors', 'captured',
                        'chased', 'compromised', 'confronted', 'contagious', 'cornered', 'culprit', 'damage',
                        'damaging', 'danger', 'dangerous', 'dangers', 'destroying', 'destructive', 'deterrent',
                        'detrimental', 'disruptive', 'electrocuted', 'eliminate', 'eliminating', 'encroachment',
                        'encrypted', 'encryption', 'epidemic', 'escape', 'escaped', 'escaping', 'expose', 'exposed',
                        'exposing', 'fatally', 'feared', 'fled', 'flee', 'fleeing', 'flu', 'foiled', 'freed', 'germ',
                        'germs', 'guarded', 'guarding', 'guards', 'gunning', 'hapless', 'harassed', 'harm', 'harmful',
                        'harmless', 'harsh', 'hepatitis', 'hid', 'hijacked', 'hijacker', 'hijackers', 'hiv', 'hostage',
                        'hostages', 'hunted', 'immune', 'immunity', 'immunization', 'imprisoned', 'improper',
                        'inadvertent', 'infect', 'infected', 'infecting', 'infection', 'infections', 'infectious',
                        'infects', 'injuring', 'intentional', 'interference', 'interfering', 'intruders', 'intrusion',
                        'intrusive', 'invaded', 'isolates', 'kidnapped', 'limiting', 'login', 'logins', 'logon',
                        'lured', 'malaria', 'malicious', 'masked', 'minimise', 'minimize', 'minimizing', 'misuse',
                        'mite', 'mitigating', 'mosquito', 'motorcade', 'nuisance', 'offending', 'outbreak', 'overrun',
                        'passcode', 'password', 'passwords', 'plaintext', 'pneumonia', 'policeman', 'potentially',
                        'prevent', 'prevented', 'preventing', 'prevents', 'prone', 'protect', 'protected', 'protecting',
                        'protection', 'protects', 'quarantine', 'raided', 'ransom', 'raped', 'refuge', 'removing',
                        'rescued', 'rescuing', 'resisting', 'risks', 'robbed', 'runaway', 'safeguard', 'secret',
                        'secrets', 'seized', 'sensitive', 'server', 'shielding', 'smallpox', 'spam', 'spores', 'stolen',
                        'stormed', 'strain', 'strains', 'stranded', 'strep', 'summoned', 'susceptible', 'swine',
                        'threat', 'threatened', 'threatening', 'threats', 'thwarted', 'tortured', 'trapped',
                        'unaccounted', 'undesirable', 'unhealthy', 'unidentified', 'unintended', 'unintentional',
                        'unnamed', 'unnecessary', 'unprotected', 'unsafe', 'unwanted', 'unwelcome', 'user', 'username',
                        'vaccine', 'vaccines', 'villagers', 'viral', 'virus', 'viruses', 'vulnerability', 'vulnerable',
                        'whereabouts', 'whooping', 'withstand', 'wounded']

SUPPORT = ['ability', 'acrobat', 'adobe', 'advantage', 'advice', 'aid', 'aids', 'aim', 'alternatives', 'app', 'apps',
           'ares', 'assist', 'autodesk', 'avs', 'benefits', 'best', 'boost', 'bring', 'bringing', 'build', 'cad',
           'cellphone', 'challenge', 'choices', 'choosing', 'compatible', 'computer', 'computers', 'conferencing',
           'console', 'consoles', 'continue', 'contribute', 'corel', 'create', 'creating', 'crucial', 'desktop',
           'desktops', 'develop', 'devices', 'digital', 'discover', 'discuss', 'ease', 'easier', 'educate', 'effective',
           'effectively', 'effort', 'electronic', 'electronics', 'encourage', 'energy', 'enhance', 'ensure',
           'essential', 'experience', 'explore', 'finding', 'future', 'gadget', 'gadgets', 'gizmos', 'goal', 'guide',
           'guides', 'handhelds', 'handset', 'handsets', 'hardware', 'help', 'helpful', 'helping', 'helps', 'hopes',
           'ideas', 'idm', 'important', 'improve', 'interactive', 'internet', 'introduce', 'invaluable', 'ios', 'join',
           'kiosk', 'kiosks', 'laptops', 'lead', 'learn', 'mac', 'machines', 'macintosh', 'macromedia', 'maintain',
           'manage', 'mcafee', 'meet', 'messaging', 'microsoft', 'mobile', 'monitors', 'mouse', 'mice', 'multimedia',
           'natural', 'needed', 'needs', 'networked', 'networking', 'norton', 'notebooks', 'novel', 'oem', 'offline',
           'office', 'opportunity', 'our', 'peripherals', 'personal', 'phone', 'phones', 'photoshop', 'plan', 'plans',
           'portables', 'potential', 'practical', 'prepare', 'pros', 'quark', 'quicken', 'recommend', 'remotely',
           'resources', 'safe', 'save', 'saving', 'screens', 'serve', 'servers', 'share', 'sharing', 'software',
           'solve', 'standalone', 'support', 'symantec', 'task', 'tech', 'telephones', 'televisions', 'their', 'tips',
           'to', 'together', 'trojan', 'useful', 'users', 'valuable', 'virtual', 'visio', 'vista', 'vital', 'vmware',
           'ways', 'working', 'workstation', 'workstations', 'xp', 'express']

ENERGY_DESK = ['amps', 'bhp', 'biomass', 'blowers', 'boiler', 'boilers', 'btu', 'burners', 'cc', 'cfm', 'chiller',
               'chillers', 'cogent', 'compressors', 'conditioner', 'conditioners', 'conditioning', 'coolers', 'cooling',
               'cranking', 'desalination', 'diesels', 'electric', 'electrical', 'electricity', 'electrification',
               'energy', 'engine', 'engines', 'furnace', 'furnaces', 'gasification', 'generators', 'geothermal', 'gpm',
               'heat', 'heater', 'heaters', 'heating', 'horsepower', 'hp', 'hvac', 'hydro', 'hydroelectric',
               'hydropower', 'idle', 'idling', 'ignition', 'interconnect', 'kilowatt', 'kilowatts', 'kw', 'liter',
               'megawatt', 'megawatts', 'motor', 'motors', 'mph', 'municipal', 'speaker', 'photovoltaic',
               'photovoltaics', 'power', 'powered', 'powerplant', 'psi', 'psig', 'reactors', 'redline', 'refrigerated',
               'refrigeration', 'renewable', 'renewables', 'power', 'powering', 'retrofits', 'retrofitting', 'revs',
               'rpm', 'siting', 'solar', 'substation', 'substations', 'switchgear', 'switch', 'temperatures', 'watt',
               'thermo', 'thermoelectric', 'thermostat', 'thermostats', 'throttle', 'torque', 'turbine', 'turbines',
               'turbo', 'underground', 'ventilation', 'volt', 'volts', 'wind', 'windmill', 'windmills']

SALES_DEPARTMENT = ['accounting', 'actual', 'advertised', 'affordable', 'auction', 'auctions', 'audited', 'auditing',
                    'bargain', 'bargains', 'bidding', 'billable', 'billed', 'billing', 'billings', 'bookkeeping',
                    'bought', 'brand', 'branded', 'brands', 'broker', 'brokerage', 'brokers', 'budgeting', 'bulk',
                    'buy', 'buyer', 'buyers', 'buying', 'buys', 'cancel', 'cancellation', 'cancellations', 'cancelled',
                    'cardholders', 'cashback', 'cashflow', 'chain', 'chargeback', 'cheap', 'cheaper', 'cheapest',
                    'checkbook', 'checkout', 'cheque', 'cheques', 'clearance', 'closeout', 'consignment', 'convenience',
                    'cosmetics', 'coupon', 'coupons', 'deals', 'debit', 'debited', 'debits', 'deducted', 'delivery',
                    'deposit', 'discontinued', 'discount', 'discounted', 'discounts', 'distributor', 'ebay', 'escrow',
                    'expensive', 'export', 'exported', 'exporter', 'exporters', 'exporting', 'exports', 'fee', 'fees',
                    'goods', 'gratuities', 'gratuity', 'groceries', 'grocery', 'import', 'importation', 'imported',
                    'importer', 'importers', 'importing', 'imports', 'incur', 'inexpensive', 'instore', 'inventory',
                    'invoice', 'invoiced', 'invoices', 'invoicing', 'item', 'items', 'lease', 'ledger', 'ledgers',
                    'manufacturer', 'marketed', 'merchandise', 'merchant', 'negotiable', 'nonmembers', 'nonrefundable',
                    'ordering', 'origination', 'outlets', 'overage', 'overdraft', 'overstock', 'owner', 'owners',
                    'payable', 'payment', 'payroll', 'postage', 'postmarked', 'premium', 'prepaid', 'prepay',
                    'prepayment', 'price', 'priced', 'prices', 'pricey', 'pricing', 'product', 'products', 'purchase',
                    'purchased', 'purchaser', 'purchases', 'purchasing', 'rebate', 'receipts', 'receivable',
                    'receivables', 'reconciliations', 'recordkeeping', 'redeem', 'redeemable', 'refund', 'refundable',
                    'refunded', 'refunding', 'refunds', 'remittance', 'resell', 'reselling', 'retail', 'retailer',
                    'retailing', 'sale', 'sell', 'seller', 'sellers', 'selling', 'sells', 'shipment', 'shipments',
                    'shipped', 'shipper', 'shippers', 'shipping', 'shop', 'shopped', 'shopping', 'shops', 'sold',
                    'spreadsheets', 'store', 'stores', 'submittal', 'supermarket', 'supermarkets', 'superstore',
                    'supplier', 'supplies', 'supply', 'surcharge', 'surcharges', 'timesheet', 'transaction', 'upfront',
                    'vendor', 'verifications', 'voucher', 'vouchers', 'warehouse', 'warehouses', 'wholesale',
                    'wholesaler']

def augument_classters(glove_path):
    embeddings_index = {}
    f = open(glove_path)
    word_counter = 0
    for line in tqdm(f):
        if word_counter == 126791:
            continue
        values = line.split()
        word = values[0]
        # difference here as we don't intersect words, we take most of them
        if (word.islower() and word.isalpha()): # work with smaller list of vectors
            coefs = np.asarray(values[1:], dtype='float32')
            embeddings_index[word] = coefs
        word_counter += 1
    f.close()

    print('Found %s word vectors matching enron data set.' % len(embeddings_index))
    print('Total words in GloVe data set: %s' % word_counter)

    # create a dataframe using the embedded vectors and attach the key word as row header
    glove_dataframe = pd.DataFrame(embeddings_index)
    glove_dataframe = pd.DataFrame.transpose(glove_dataframe)

    departments = [LEGAL, COMMUICATIONS, SECURITY_SPAM_ALERTS, SUPPORT, ENERGY_DESK,  SALES_DEPARTMENT]

    temp_matrix = pd.DataFrame.as_matrix(glove_dataframe)

    vocab_boost_count = 5
    for group_id in range(len(departments)):
        print('Working bag number:', str(group_id))
        glove_dataframe_temp = glove_dataframe.copy()
        vocab = []
        for word in departments[group_id]:
            vocab.append(word)
            cos_dist_rez = scipy.spatial.distance.cdist(temp_matrix, np.array(glove_dataframe.loc[word])[np.newaxis,:], metric='cosine')
            # find closest words to help
            glove_dataframe_temp['cdist'] = cos_dist_rez
            glove_dataframe_temp = glove_dataframe_temp.sort_values(['cdist'], ascending=[1])
            vocab = vocab + list(glove_dataframe_temp.head(vocab_boost_count).index)
        # replace boosted set to old department group and remove duplicates
        departments[group_id] = list(set(vocab))
    return departments
