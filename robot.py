import tensorflow as tf
import math
import numpy as np
import json
import re
import pandas as pd
import datetime
import scipy
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

COLUMNS = ['content_length',
               'content_word_count',
               'group_LEGAL',
               'group_COMMUICATIONS',
               'group_SECURITY_SPAM_ALERTS',
               'group_SUPPORT',
               'group_ENERGY_DESK',
               'group_SALES_DEPARTMENT',
               'is_AM',
               'is_weekday',
               'subject_length',
               'subject_word_count',
               'outcome']

LABEL_COLUMN = 'outcome'
CATEGORICAL_COLUMNS = ["is_AM", "is_weekday"]
CONTINUOUS_COLUMNS = ['content_length',
                          'content_word_count',
                          'group_LEGAL',
                          'group_COMMUICATIONS',
                          'group_SECURITY_SPAM_ALERTS',
                          'group_SUPPORT',
                          'group_ENERGY_DESK',
                          'group_SALES_DEPARTMENT',
                          'subject_length',
                          'subject_word_count']

LABELS = [0, 1, 2, 3, 4, 5]
groups_order = ['LEGAL','COMMUNICATIONS','SECURITY_SPAM_ALERTS', 'SUPPORT', 'ENERGY_DESK','SALES_DEPARTMENT']

def input_fn(df):
    # Creates a dictionary mapping from each continuous feature column name (k) to
    # the values of that column stored in a constant Tensor.
    continuous_cols = {k: tf.constant(df[k].values)
                       for k in CONTINUOUS_COLUMNS}
    # Creates a dictionary mapping from each categorical feature column name (k)
    # to the values of that column stored in a tf.SparseTensor.
    categorical_cols = {k: tf.SparseTensor(
        indices=[[i, 0] for i in range(df[k].size)],
        values=df[k].values,
        dense_shape=[df[k].size, 1])
        for k in CATEGORICAL_COLUMNS}
    # Merges the two dictionaries into one.
    feature_cols = {}
    feature_cols.update(continuous_cols)
    feature_cols.update(categorical_cols)

    # Converts the label column into a constant Tensor.
    label = tf.constant(df[LABEL_COLUMN].values)
    # Returns the feature columns and the label.
    return feature_cols, label


def cos_dist(vec1, vec2):
    assert (len(vec1) == len(vec2))
    summ = 0
    for i in range(0, len(vec1)):
        summ += (vec1[i] - vec2[i]) ** 2
    return math.sqrt(summ)


class EmailRobot:
    def __init__(self, nn_path, deparments_path, glove_path):
        self.classifier = self.__load_nn(nn_path)

        self.glove_path = glove_path
        self.departments, self.departments_names = self.__load_departments(deparments_path)
        self.departments_embeddings = []
        for department in self.departments:
            department_embed = self.__find_embeddings(department)
            self.departments_embeddings.append(department_embed)
        self.small_glove_embeddings_index = self.__load_small_glove()
        # load_departments
        # load_nn

    def __load_departments(self, departments_path):
        departments = []
        names = []
        with open(departments_path) as json_file:
            data = json.load(json_file)
            deps_dict = {}
            for department in data:
                deps_dict[department] = data[department]
                #departments.append(data[department])
                #names.append(department)

        for i, name in enumerate(groups_order):
            departments.append(deps_dict[name])
            names.append(name)
        names.append('None')
        return departments, names

    def __load_nn(self, nn_path):
        content_length = tf.contrib.layers.real_valued_column("content_length")
        content_word_count = tf.contrib.layers.real_valued_column("content_word_count")
        subject_length = tf.contrib.layers.real_valued_column("subject_length")
        subject_word_count = tf.contrib.layers.real_valued_column("subject_word_count")
        group_LEGAL = tf.contrib.layers.real_valued_column("group_LEGAL")
        group_COMMUICATIONS = tf.contrib.layers.real_valued_column("group_COMMUICATIONS")
        group_SECURITY_SPAM_ALERTS = tf.contrib.layers.real_valued_column("group_SECURITY_SPAM_ALERTS")
        group_SUPPORT = tf.contrib.layers.real_valued_column("group_SUPPORT")
        group_ENERGY_DESK = tf.contrib.layers.real_valued_column("group_ENERGY_DESK")
        group_SALES_DEPARTMENT = tf.contrib.layers.real_valued_column("group_SALES_DEPARTMENT")
        content_length_bucket = tf.contrib.layers.bucketized_column(content_length, boundaries=[100, 200, 300, 400])
        subject_length_bucket = tf.contrib.layers.bucketized_column(subject_length, boundaries=[10, 15, 20, 25, 30])

        # Categorical base columns
        is_AM_sparse_column = tf.contrib.layers.sparse_column_with_keys(column_name="is_AM", keys=["yes", "no"])
        is_weekday_sparse_column = tf.contrib.layers.sparse_column_with_keys(column_name="is_weekday",
                                                                             keys=["yes", "no"])

        categorical_columns = [is_AM_sparse_column, is_weekday_sparse_column, content_length_bucket,
                               subject_length_bucket]

        deep_columns = [content_length, content_word_count, subject_length, subject_word_count,
                        group_LEGAL, group_COMMUICATIONS, group_SECURITY_SPAM_ALERTS, group_SUPPORT,
                        group_ENERGY_DESK, group_SALES_DEPARTMENT]
        classifier = tf.contrib.learn.DNNClassifier(feature_columns=deep_columns,
                                                    hidden_units=[20, 20],
                                                    n_classes=6,
                                                    model_dir=nn_path)
        return classifier

    def letter_classifier(self, email):
        result, recognizible = self.scrub_text(email['Subject'], email['Content'])
        if recognizible is None:
            return self.departments_names[-1]
        y_pred = self.classifier.predict(input_fn=lambda: input_fn(result), as_iterable=False)
        return self.departments_names[y_pred[0]]

    def unrecognized_letter(self, letter, target_group):
        traget_group_id = -1
        for i, name in enumerate(self.departments_names):
            if name == target_group:
                target_group_id = i
                break
        assert(target_group_id != -1)
        subject_to_predict = letter['Subject'].lower()
        pattern = re.compile('[^a-z]')
        subject_to_predict = re.sub(pattern, ' ', subject_to_predict)
        pattern = re.compile('\s+')
        subject_to_predict = re.sub(pattern, ' ', subject_to_predict)

        content_to_predict = letter['Content'].lower()
        pattern = re.compile('[^a-z]')
        content_to_predict = re.sub(pattern, ' ', content_to_predict)
        pattern = re.compile('\s+')
        content_to_predict = re.sub(pattern, ' ', content_to_predict)

        text = subject_to_predict + ' ' + content_to_predict
        words = text.split()

        embeddings_index = self.__find_embeddings(words)

        # create a dataframe using the embedded vectors and attach the key word as row header
        glove_dataframe = pd.DataFrame(self.departments_embeddings[target_group_id])
        glove_dataframe = pd.DataFrame.transpose(glove_dataframe)
        temp_matrix = pd.DataFrame.as_matrix(glove_dataframe)

        distances = {}
        for word in embeddings_index:
            min_dist = None
            for departments_word in self.departments_embeddings[target_group_id]:
                dist = cos_dist(embeddings_index[word], self.departments_embeddings[target_group_id][departments_word])
                if min_dist is None or dist < min_dist:
                    min_dist = dist
            distances[word] = min_dist

        sorted_distances = sorted(distances.items(), key=lambda x: x[1])
        clothest_word = sorted_distances[0][0]
        auguments = self.__augument_embbeded(clothest_word, self.departments[target_group_id])
        print(clothest_word)
        print(distances[clothest_word])
        print(auguments)
        self.departments[target_group_id] += auguments
        self.departments_embeddings[target_group_id][clothest_word] = embeddings_index[clothest_word]

    def __find_embeddings(self, words):
        embeddings_index = {}
        f = open(self.glove_path)
        for line in f:
            values = line.split()
            word = values[0]
            if word in words:
                try:
                    coefs = np.asarray(values[1:], dtype='float32')
                except Exception:
                    continue
                embeddings_index[word] = coefs
        f.close()
        return embeddings_index

    def __augument_embbeded(self, target_word, department, vocab_boost_count=5):
        glove_dataframe = pd.DataFrame(self.small_glove_embeddings_index)
        glove_dataframe = pd.DataFrame.transpose(glove_dataframe)
        temp_matrix = pd.DataFrame.as_matrix(glove_dataframe)
        glove_dataframe_temp = glove_dataframe.copy()
        #for word in department:
        cos_dist_rez = scipy.spatial.distance.cdist(temp_matrix,
                                                        np.array(glove_dataframe.loc[target_word])[np.newaxis, :],
                                                        metric='cosine')
            # find closest words to help
        glove_dataframe_temp['cdist'] = cos_dist_rez
        glove_dataframe_temp = glove_dataframe_temp.sort_values(['cdist'], ascending=[1])
        vocab = list(glove_dataframe_temp.head(vocab_boost_count).index)
        return vocab

    def __load_small_glove(self):
        glove_embeddings_index = {}
        f = open(self.glove_path)
        for line in f:
            values = line.split()
            word = values[0]
            # difference here as we don't intersect words, we take most of them
            if (word.islower() and word.isalpha()):  # work with smaller list of vectors
                try:
                    coefs = np.asarray(values[1:], dtype='float32')
                except Exception:
                    continue
                glove_embeddings_index[word] = coefs
        f.close()
        return glove_embeddings_index

    def __input_fn(self, df):
        # Creates a dictionary mapping from each continuous feature column name (k) to
        # the values of that column stored in a constant Tensor.
        continuous_cols = {k: tf.constant(df[k].values)
                           for k in CONTINUOUS_COLUMNS}
        # Creates a dictionary mapping from each categorical feature column name (k)
        # to the values of that column stored in a tf.SparseTensor.
        categorical_cols = {k: tf.SparseTensor(
            indices=[[i, 0] for i in range(df[k].size)],
            values=df[k].values,
            dense_shape=[df[k].size, 1])
            for k in CATEGORICAL_COLUMNS}
        # Merges the two dictionaries into one.
        # feature_cols = dict(continuous_cols.items() + categorical_cols.items())
        feature_cols = {}
        feature_cols.update(continuous_cols)
        feature_cols.update(categorical_cols)

        # Converts the label column into a constant Tensor.
        label = tf.constant(df[LABEL_COLUMN].values)
        # Returns the feature columns and the label.
        return feature_cols, label

    def scrub_text(self, subject_to_predict, content_to_predict):
        recognizible = True
        # prep text
        subject_to_predict = subject_to_predict.lower()
        pattern = re.compile('[^a-z]')
        subject_to_predict = re.sub(pattern, ' ', subject_to_predict)
        pattern = re.compile('\s+')
        subject_to_predict = re.sub(pattern, ' ', subject_to_predict)

        content_to_predict = content_to_predict.lower()
        pattern = re.compile('[^a-z]')
        content_to_predict = re.sub(pattern, ' ', content_to_predict)
        pattern = re.compile('\s+')
        content_to_predict = re.sub(pattern, ' ', content_to_predict)

        # get bag-of-words
        words_groups = []
        text = subject_to_predict + ' ' + content_to_predict
        for group_id in range(len(self.departments)):
            work_group = []
            top_words = self.departments[group_id]
            work_group.append(len([w for w in text.split() if w in set(top_words)]))
            words_groups.append(work_group)

        # count emails per category group and feature engineering
        raw_text = []
        subject_length = []
        subject_word_count = []
        content_length = []
        content_word_count = []
        is_am_list = []
        is_weekday_list = []
        group_LEGAL = []
        group_COMMUICATIONS = []
        group_SECURITY_SPAM_ALERTS = []
        group_SUPPORT = []
        group_ENERGY_DESK = []
        group_SALES_DEPARTMENT = []
        final_outcome = []

        cur_time_stamp = datetime.datetime.now()

        raw_text.append(text)
        group_LEGAL.append(words_groups[0])
        group_COMMUICATIONS.append(words_groups[1])
        group_SECURITY_SPAM_ALERTS.append(words_groups[2])
        group_SUPPORT.append(words_groups[3])
        group_ENERGY_DESK.append(words_groups[4])
        group_SALES_DEPARTMENT.append(words_groups[5])

        outcome_tots = [words_groups[0], words_groups[1], words_groups[2], words_groups[3], words_groups[4],
                        words_groups[5]]
        final_outcome.append(outcome_tots.index(max(outcome_tots)))

        if group_LEGAL[0] == 0 and group_COMMUICATIONS[0]  == 0 and group_SECURITY_SPAM_ALERTS[0]  == 0 \
                and group_SUPPORT[0]  == 0 and group_ENERGY_DESK[0]  == 0 and group_SALES_DEPARTMENT[0] == 0:
            recognizible = False

        subject_length.append(len(subject_to_predict))
        subject_word_count.append(len(subject_to_predict.split()))
        content_length.append(len(content_to_predict))
        content_word_count.append(len(content_to_predict.split()))
        is_am_list.append('no')
        is_weekday_list.append('no')
        # add simple engineered features?
        training_set = pd.DataFrame({
            "raw_text": raw_text,
            "group_LEGAL": group_LEGAL[0],
            "group_COMMUICATIONS": group_COMMUICATIONS[0],
            "group_SECURITY_SPAM_ALERTS": group_SECURITY_SPAM_ALERTS[0],
            "group_SUPPORT": group_SUPPORT[0],
            "group_ENERGY_DESK": group_ENERGY_DESK[0],
            "group_SALES_DEPARTMENT": group_SALES_DEPARTMENT[0],
            "subject_length": subject_length,
            "subject_word_count": subject_word_count,
            "content_length": content_length,
            "content_word_count": content_word_count,
            "is_AM": is_am_list,
            "is_weekday": is_weekday_list,
            "outcome": final_outcome})
        return (training_set), recognizible

robot = EmailRobot('/home/vlad/neurokris', '/home/vlad/departments.json', '/home/vlad/glove.840B.300d.txt')
print(robot.letter_classifier({'Subject': 'Computer troubles' ,'Content':'I have some troubles with copmuter. Need your help'}))
print(robot.letter_classifier({'Subject': 'New employer' ,'Content':'Hi, could you make some documetns for our new employer'}))
print(robot.letter_classifier({'Subject': 'Price' ,'Content':'Good evening! Can u tell me a price for something'}))
#print(robot.letter_classifier({'Subject': 'Computer troubles' ,'Content':'I have some troubles with copmuter. Need your help'}))
#robot.unrecognized_letter({'Subject': 'Computer troubles' ,'Content':'I have some troubles with copmuter. Need your help'}, 'SUPPORT')